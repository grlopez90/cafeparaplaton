<div class="container Header-wrapper">	
	<div class="row Header-border">
		<div class="col-md-2 col-xs-12">
			<a href="#" class="Header-logo">
				<?= $this->Html->image('logo.png'); ?>
			</a>

			<input type="checkbox" id="HamburgerCheck" class="Header-hamburgerCheck" autocomplete="off">
			<label for="HamburgerCheck" class="Header-hamburgerLabel">
				<i class="fas fa-bars"></i>
			</label>

			<nav class="MainMenu MobileMenu">
				<ul class="MainMenu-container">
					<li class="MainMenu-option MobileMenu-option">
						<a href="/" class="MainMenu-link">Inicio</a>
					</li>								
					<li class="MainMenu-option MobileMenu-option">
						<a href="/posts" class="MainMenu-link">Artículos</a>
					</li>
					<li class="MainMenu-option MobileMenu-option">
						<a href="/contact" class="MainMenu-link">Contacto</a>
					</li>
				</ul>
			</nav>
		</div>
		<div class="d-none d-md-block col-md-10">				
			<nav class="MainMenu">
				<ul class="MainMenu-container">
					<li class="MainMenu-option">
						<a href="/" class="MainMenu-link">Inicio</a>
					</li>								
					<li class="MainMenu-option">
						<a href="/posts" class="MainMenu-link">Artículos</a>
					</li>
					<li class="MainMenu-option">
						<a href="/contact" class="MainMenu-link">Contacto</a>
					</li>
				</ul>
			</nav>													
		</div>
	</div>
	
</div>
