<div class="Footer-wrapper">
	<div class="container">
		<div class="row">
			<div class="col-xs-12 col-md-6 Footer-column">
				<figure class="Footer-logo">
					<img src="/img/logo-white.png">
				</figure>			
			</div>
			<div class="col-xs-12 col-md-6 Footer-column">
				<p class="Footer-info">
					Lorem ipsum dolor sit amet consectetur adipisicing elit. Exercitationem optio, autem qui consequatur saepe veniam explicabo rem? Repellendus quasi sit porro ipsam, atque odit quo in quibusdam architecto officiis reiciendis!Omnis sit consequuntur, magnam corporis corrupti iure ullam maxime a recusandae dolorum vero perferendis quidem neque officiis vitae porro deleniti?
				</p>
				<p class="Footer-info">
					Lorem ipsum dolor sit amet consectetur adipisicing elit. Exercitationem optio, autem qui consequatur saepe veniam explicabo rem? Repellendus quasi sit porro ipsam, atque odit quo in quibusdam architecto officiis reiciendis!Omnis sit consequuntur, magnam corporis corrupti iure ullam maxime a recusandae dolorum vero perferendis quidem neque officiis vitae porro deleniti?
				</p>
			</div>		
		</div>
		<div class="Footer-social">
			<a href="#" class="Footer-facebook">
				<i class="fab fa-facebook-square"></i>
			</a>
		</div>
	</div>	
</div>
