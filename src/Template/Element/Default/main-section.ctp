
<section class="MainSection" style="background: url('<?= "/files/posts/photo/{$mainPost->photo_dir}/{$mainPost->photo}" ?>') #CCC; background-position: center">
    <div class="white-overlay">
        <div class="container">
            <div class="Header-plate">
                <div class="Header-plateContent">
                    <div class="Header-plateScrew top-left"></div>
                    <div class="Header-plateScrew top-right"></div>
                    <div class="Header-plateScrew bot-left"></div>
                    <div class="Header-plateScrew bot-right"></div>
                    <a href="<?= "/{$mainPost->category->name}/{$mainPost->permalink}" ?>">
                        <h1 class="Header-plateTitle"><?= @$mainPost->name ?></h1>
                    </a>
                </div>
            </div>
        </div>
    </div>
</section>