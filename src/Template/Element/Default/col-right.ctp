<aside class="u-wrapper block-wrapper">
	<h3 class="title brown-title">Últimos Tags</h2>
	<div class="Tags" id="tags">
		<ul class="Tags-list">
			<li class="Tag-item" v-for="tag in tags">
				<a :href="'/tags/' + tag.permalink" class="Tag-link">{{ tag.name }}</a>
			</li>
		</ul>
	</div>
</aside>

<aside class="u-wrapper block-wrapper">
	<h3 class="title brown-title">Síguenos</h2>
	<div id="fb-root"></div>
	<script>(function(d, s, id) {
	var js, fjs = d.getElementsByTagName(s)[0];
	if (d.getElementById(id)) return;
	js = d.createElement(s); js.id = id;
	js.src = 'https://connect.facebook.net/en_GB/sdk.js#xfbml=1&version=v2.11&appId=1612766149007247';
	fjs.parentNode.insertBefore(js, fjs);
	}(document, 'script', 'facebook-jssdk'));</script>

	<div class="fb-page" data-href="https://www.facebook.com/BPOinsight/" data-tabs="timeline" data-small-header="true" data-adapt-container-width="true" data-hide-cover="false" data-show-facepile="true"><blockquote cite="https://www.facebook.com/BPOinsight/" class="fb-xfbml-parse-ignore"><a href="https://www.facebook.com/BPOinsight/">BPOinsight</a></blockquote></div>

</aside>

<aside class="u-wrapper block-wrapper Tabs-block">
	<div class="Tabs">
		<ul class="nav nav-tabs" id="myTab" role="tablist">
			<li class="nav-item Tabs-tabHead">
				<a class="nav-link active" id="home-tab" data-toggle="tab" href="#home" role="tab" aria-controls="home" aria-selected="true">Últimos</a>
			</li>
			<li class="nav-item Tabs-tabHead">
				<a class="nav-link" id="profile-tab" data-toggle="tab" href="#profile" role="tab" aria-controls="profile" aria-selected="false">Populares</a>
			</li>
		</ul>
		<div class="TabsContent-wrapper">
			<div class="tab-content" id="myTabContent">				
				<div class="tab-pane fade show active" id="profile" role="tabpanel" aria-labelledby="profile-tab">
					<div class="row" v-for="last in lastest">
						<div class="col-md-4">
							<figure class="TabContent-image-container">
								<img :src="'/files/posts/photo/' + last.photo_dir + '/' + last.photo">
							</figure>
						</div>
						<div class="col-md">
							<p class="TabContent-extract">
								{{ last.name }}
							</p>
						</div>
					</div>
				</div>
				<div class="tab-pane fade" id="populars" role="tabpanel" aria-labelledby="home-tab">
					<div class="row" v-for="popular in populars">
						<div class="col-md-4">
							<figure class="TabContent-image-container">
								<img :src="'/files/posts/photo/' + popular.photo_dir + '/' + popular.photo">
							</figure>
						</div>
						<div class="col-md">
							<p class="TabContent-extract">
								{{ popular.name }}
							</p>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>	
</aside>