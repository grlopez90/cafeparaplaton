<section class="LayoutContent">
    <div class="Content-container container">
        <div class="row">
            <div class="col-md-8">
                <?php foreach ($posts as $post): ?>
                <article class="Post u-wrapper">
                    <div class="Post-header">
                        <h3 class="Post-title">
                            <a href="<?= $this->Url->build("/{$post->category->permalink}/{$post->permalink}"); ?>"><?= $post->name ?></a>
                        </h2>
                        <div class="Post-info">
                            <p class="Post-author"><?= $post->author ?></p>
                            <p class="Post-date"><?= $this->Time->nice($post->created); ?></p>
                        </div>
                    </div>
                    <div class="Post-body">
                        <figure class="Post-image">
                            <a href="<?= $this->Url->build('/'); ?>">
                                <?php
                                $src = '/files/posts/photo/'.$post->photo_dir.'/'.$post->photo;
                                if (!file_exists(WWW_ROOT.$src)) 
                                    $src = '/img/post-example.png';
                                ?>
                                <img src="<?= $src ?>" width="100%">
                            </a>
                        </figure>
                        <div class="Post-bodyContent">
                            <div class="u-post-content-wrapper">
                                <?= $post->intro ?>
                            </div>
                        </div>
                    </div>
                    <div class="Post-footer">
                        <div class="row">
                            <div class="col-md col-sm col-xs">
                                <a href="<?= $this->Url->build("/{$post->category->permalink}/{$post->permalink}"); ?>" class="Post-link">READ MORE</a>
                            </div>
                            <div class="col-md col-sm col-xs">
                                <ul class="Post-social">
                                    <li class="Post-social-item">
                                        SHARE:
                                    </li>
                                    <li class="Post-social-item">
                                        <a href="http://www.facebook.com/sharer.php?u=http://bpoinsight.com/<?=@$post->category->permalink.'/'.@$post->permalink ?>" onclick="window.open(this.href, this.target, 'width=550,height=415'); return false;">
                                            <i class="fab fa-facebook-square" aria-hidden="true"></i>
                                        </a>
                                    </li>
                                    <li class="Post-social-item">|</li>
                                    <li class="Post-social-item">
                                        <a href="https://twitter.com/intent/tweet?text=<?= $post->name ?>&url=http://bpoinsight,com/<?= $post->category->permalink.'/'.$post->permalink ?>&via=bpoinsight" target="_blank" onclick="window.open(this.href, this.target, 'width=550,height=415'); return false;">
                                            <i class="fab fa-twitter-square" aria-hidden="true"></i>
                                        </a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </article>  
                <?php endforeach ?>
            </div>
            <div class="col-md-4">
                <?= $this->element('Default/col-right'); ?>
            </div>
        </div>
    </div>
</section>