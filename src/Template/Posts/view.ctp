<?php $this->assign('title', $post->name); ?>
<section class="LayoutContent">
    <div class="Content-container container">
        <div class="row">
            <div class="col-md-8">
                <article class="Post u-wrapper">
                    <div class="Post-header">
                        <figure class="Post-image">
                            <?php
                            $src = '/files/posts/photo/'.$post->photo_dir.'/'.$post->photo;
                            if (!file_exists(WWW_ROOT.$src)) 
                                $src = '/img/post-example.png';
                            ?>
                            <img src="<?= $src ?>" width="100%">
                        </figure>
                        <h3 class="Post-title brown-title"><?= $post->name ?></h2>
                        <div class="Post-info">
                            <p class="Post-author"><?= $post->author ?></p>
                            <p class="Post-date"><?= $this->Time->nice($post->created); ?></p>
                        </div>
                    </div>
                    <div class="Post-body">
                        <div class="Post-bodyContent">
                            <div class="u-post-content-wrapper">
                                <?= $post->description ?>
                            </div>
                        </div>
                    </div>
                    <div class="Post-footer">
                        <div class="row">
                            <div class="col-md col-sm col-xs">
                                <ul class="Post-social">
                                    <li class="Post-social-item">
                                        Compartir:
                                    </li>
                                    <li class="Post-social-item">
                                        <a href="http://www.facebook.com/sharer.php?u=http://bpoinsight.com/<?=@$post->category->permalink.'/'.@$post->permalink ?>" onclick="window.open(this.href, this.target, 'width=550,height=415'); return false;">
                                            <i class="fab fa-facebook-square" aria-hidden="true"></i>
                                        </a>
                                    </li>
                                    <li class="Post-social-item">|</li>
                                    <li class="Post-social-item">
                                        <a href="https://twitter.com/intent/tweet?text=<?= $post->twitter_title ?>&url=http://bpoinsight,com/<?= $post->category->permalink.'/'.$post->permalink ?>&via=bpoinsight" target="_blank" onclick="window.open(this.href, this.target, 'width=550,height=415'); return false;">
                                            <i class="fab fa-twitter-square" aria-hidden="true"></i>
                                        </a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </article>  
            </div>
            <div class="col-md-4">
                <?= $this->element('Default/col-right'); ?>
            </div>
        </div>
    </div>
</section>
