<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Category[]|\Cake\Collection\CollectionInterface $categories
 */
?>
<nav class="large-3 medium-4 columns nav-actions" id="actions-sidebar">
    <ul class="nav nav-pills">
        <li class="nav-item"><?= $this->Html->link(__('New Category'), ['action' => 'add'],['class'=>'nav-link btn btn-info']) ?></li>
        <li class="nav-item"><?= $this->Html->link(__('List Posts'), ['controller' => 'Posts', 'action' => 'index'],['class'=>'nav-link btn btn-info']) ?></li>
        <li class="nav-item"><?= $this->Html->link(__('New Post'), ['controller' => 'Posts', 'action' => 'add'],['class'=>'nav-link btn btn-info']) ?></li>
    </ul>
</nav>

<div class="card">
    <div class="card-header">
        <strong>Categories</strong>
        <small>Lits</small>
    </div>
    <div class="card-body">
        <div class="categories index large-9 medium-8 columns content">
            <table cellpadding="0" cellspacing="0" class="table">
                <thead>
                    <tr>
                        <th scope="col"><?= $this->Paginator->sort('id') ?></th>
                        <th scope="col"><?= $this->Paginator->sort('name') ?></th>
                        <th scope="col"><?= $this->Paginator->sort('permalink') ?></th>
                        <th scope="col"><?= $this->Paginator->sort('created') ?></th>
                        <th scope="col"><?= $this->Paginator->sort('modified') ?></th>
                        <th scope="col" class="actions"><?= __('Actions') ?></th>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach ($categories as $category): ?>
                    <tr>
                        <td><?= $this->Number->format($category->id) ?></td>
                        <td><?= h($category->name) ?></td>
                        <td><?= h($category->permalink) ?></td>
                        <td><?= h($category->created) ?></td>
                        <td><?= h($category->modified) ?></td>
                        <td class="actions">
                            <?= $this->Html->link(__('Edit'), ['action' => 'edit', $category->id],['class'=>'btn btn-primary btn-sm']) ?>
                            <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $category->id], ['confirm' => __('Are you sure you want to delete # {0}?', $category->id),'class'=>'btn btn-danger btn-sm']) ?>
                        </td>
                    </tr>
                    <?php endforeach; ?>
                </tbody>
            </table>
            <div class="paginator">
                <ul class="pagination">
                    <?= $this->Paginator->first('<< ' . __('first')) ?>
                    <?= $this->Paginator->prev('< ' . __('previous')) ?>
                    <?= $this->Paginator->numbers() ?>
                    <?= $this->Paginator->next(__('next') . ' >') ?>
                    <?= $this->Paginator->last(__('last') . ' >>') ?>
                </ul>
                <p><?= $this->Paginator->counter(['format' => __('Page {{page}} of {{pages}}, showing {{current}} record(s) out of {{count}} total')]) ?></p>
            </div>
        </div>
    </div>
</div>
