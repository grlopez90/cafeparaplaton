<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Post $post
 */
?>
<nav class="large-3 medium-4 columns nav-actions">
    <ul class="nav nav-pills">
        <li class="nav-item"><?= $this->Html->link(__('List Post'), ['action' => 'index'],['class'=>'nav-link tn btn-info']) ?></li>
        <li class="nav-item"><?= $this->Html->link(__('List Categories'), ['controller' => 'Categories', 'action' => 'index'],['class'=>'nav-link tn btn-info']) ?></li>
        <li class="nav-item"><?= $this->Html->link(__('New Category'), ['controller' => 'Categories', 'action' => 'add'],['class'=>'nav-link tn btn-info']) ?></li>
    </ul>
</nav>
<div class="card">
    <?= $this->Form->create($post,['type' => 'file']) ?>
    <div class="card-header">
        <strong>Add</strong>
        <small>
            Post
        </small>
    </div>
    <div class="card-body">
        <div class="container">
            <div class="row">
                <div class="col-md-6">
                    <?= $this->Form->control('name',['class'=>'form-control']); ?>
                </div>    
                <div class="col-md-6">
                    <?= $this->Form->control('category_id', ['options' => $categories,'class'=>'form-control']); ?>
                </div>    
                <div class="col-md-12">
                    <?= $this->Form->control('description',['class'=>'form-control tinymce']); ?>
                </div>  
                <div class="col-md-12">
                    <?= $this->Form->control('author',['class'=>'form-control']); ?>
                </div>    
                <div class="col-md-12">
                    <?= $this->Form->control('intro',['class'=>'form-control']); ?>
                </div>  
                <div class="col-md-6">
                    <?= $this->Form->control('tags',['class'=>'form-control chosen','options'=>$tags,'multiple'=>true]); ?>
                </div>    
                <div class="col-md-6">
                    <?= $this->Form->control('caption_image',['class'=>'form-control']); ?>
                </div>   
                <div class="col-md-6">
                    <?= $this->Form->control('meta_title',['class'=>'form-control']); ?>
                    <?= $this->Form->control('meta_description',['class'=>'form-control']); ?>
                </div>
                <div class="col-md-6">
                    <?= $this->Form->control('facebook_title',['class'=>'form-control']); ?>
                    <?= $this->Form->control('facebook_description',['class'=>'form-control']); ?>
                </div> 
                <div class="col-md-6">
                    <?= $this->Form->control('twitter_title',['class'=>'form-control']); ?>
                    <?= $this->Form->control('twitter_description',['class'=>'form-control']); ?>
                </div>    
                <div class="col-md-6">
                    <?= $this->Form->control('photo',['class'=>'form-control','type'=>'file']); ?>
                </div>    
            </div>
        </div>
    </div>
    <div class="card-footer">
        <?= $this->Form->button('Submit',['class'=>'btn btn-success']) ?>
    </div>
    <?= $this->Form->end() ?>
</div>
