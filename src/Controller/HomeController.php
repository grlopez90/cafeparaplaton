<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\Event\Event;

/**
 * Home Controller
 *
 *
 * @method \App\Model\Entity\Home[] paginate($object = null, array $settings = [])
 */
class HomeController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
        $this->loadModel('Posts');
        $mainPost = $this->Posts->find()
                    ->order(['Posts.created'=>'DESC'])
                    ->contain(['Categories'])
                    ->first();

        $options = [
            'contain' => ['Categories'],
            'order' => ['Posts.created' => 'DESC'],
            'conditions' => ['Posts.id !=' => @$mainPost->id]
        ];
        $posts = $this->Posts->find('all',$options);

        $description_for_layout = 'We are a group of professionals with 20+ years of experience in the OUTSOURCING of SERVICES and PROCESSES in mainland United States as well as in other countries with BPO centered economies. This Website was created to give sufficient insight to those who want to optimize productivity vs investment. BPOinsight.com is not affiliated with any company and our blog is done independently & by invitation.';
        $keywords = 'bpo,outsourcing,Business process outsourcing,Business,process';

        $this->set(['description_for_layout' => $description_for_layout, 'keywords' => $keywords]);

        $this->set(compact('posts'));
        $this->set(compact('mainPost'));
    }

    /**
     * View method
     *
     * @param string|null $id Home id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $home = $this->Home->get($id, [
            'contain' => []
        ]);

        $this->set('home', $home);
        $this->set('_serialize', ['home']);
    }

    
    public function beforeFilter(Event $event)
    {
        parent::beforeFilter($event);
        // Allow users to register and logout.
        // You should not add the "login" action to allow list. Doing so would
        // cause problems with normal functioning of AuthComponent.
        $this->Auth->allow(['index', 'logout']);
    }

}
