<?php
use Migrations\AbstractSeed;
use \Cake\Auth\DefaultPasswordHasher;
use Cake\I18n\Time;

/**
 * Users seed.
 */
class UsersSeed extends AbstractSeed
{
    /**
     * Run Method.
     *
     * Write your database seeder using this method.
     *
     * More information on writing seeds is available here:
     * http://docs.phinx.org/en/latest/seeding.html
     *
     * @return void
     */
    public function run()
    {
        $data = [
            [
                'username' => 'admin',
                'password' => (new DefaultPasswordHasher)->hash('admin'),
                'role' => 'admin',
                'created' => Time::now(),
                'modified' => Time::now()
            ]
        ];

        $table = $this->table('users');
        $table->insert($data)->save();
    }
}
